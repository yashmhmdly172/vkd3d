#include "vkd3d_shader_private.h"

#include <stdarg.h>
#include <stdio.h>

static const char * const shader_opcode_names[] =
{
    [VKD3DSIH_ABS                             ] = "abs",
    [VKD3DSIH_ACOS                            ] = "acos",
    [VKD3DSIH_ADD                             ] = "add",
    [VKD3DSIH_AND                             ] = "and",
    [VKD3DSIH_ASIN                            ] = "asin",
    [VKD3DSIH_ATAN                            ] = "atan",
    [VKD3DSIH_ATOMIC_AND                      ] = "atomic_and",
    [VKD3DSIH_ATOMIC_CMP_STORE                ] = "atomic_cmp_store",
    [VKD3DSIH_ATOMIC_IADD                     ] = "atomic_iadd",
    [VKD3DSIH_ATOMIC_IMAX                     ] = "atomic_imax",
    [VKD3DSIH_ATOMIC_IMIN                     ] = "atomic_imin",
    [VKD3DSIH_ATOMIC_OR                       ] = "atomic_or",
    [VKD3DSIH_ATOMIC_UMAX                     ] = "atomic_umax",
    [VKD3DSIH_ATOMIC_UMIN                     ] = "atomic_umin",
    [VKD3DSIH_ATOMIC_XOR                      ] = "atomic_xor",
    [VKD3DSIH_BEM                             ] = "bem",
    [VKD3DSIH_BFI                             ] = "bfi",
    [VKD3DSIH_BFREV                           ] = "bfrev",
    [VKD3DSIH_BRANCH                          ] = "branch",
    [VKD3DSIH_BREAK                           ] = "break",
    [VKD3DSIH_BREAKC                          ] = "breakc",
    [VKD3DSIH_BREAKP                          ] = "breakp",
    [VKD3DSIH_BUFINFO                         ] = "bufinfo",
    [VKD3DSIH_CALL                            ] = "call",
    [VKD3DSIH_CALLNZ                          ] = "callnz",
    [VKD3DSIH_CASE                            ] = "case",
    [VKD3DSIH_CHECK_ACCESS_FULLY_MAPPED       ] = "check_access_fully_mapped",
    [VKD3DSIH_CMP                             ] = "cmp",
    [VKD3DSIH_CND                             ] = "cnd",
    [VKD3DSIH_CONTINUE                        ] = "continue",
    [VKD3DSIH_CONTINUEP                       ] = "continuec",
    [VKD3DSIH_COUNTBITS                       ] = "countbits",
    [VKD3DSIH_CRS                             ] = "crs",
    [VKD3DSIH_CUT                             ] = "cut",
    [VKD3DSIH_CUT_STREAM                      ] = "cut_stream",
    [VKD3DSIH_DADD                            ] = "dadd",
    [VKD3DSIH_DCL                             ] = "dcl",
    [VKD3DSIH_DCL_CONSTANT_BUFFER             ] = "dcl_constantBuffer",
    [VKD3DSIH_DCL_FUNCTION_BODY               ] = "dcl_function_body",
    [VKD3DSIH_DCL_FUNCTION_TABLE              ] = "dcl_function_table",
    [VKD3DSIH_DCL_GLOBAL_FLAGS                ] = "dcl_globalFlags",
    [VKD3DSIH_DCL_GS_INSTANCES                ] = "dcl_gs_instances",
    [VKD3DSIH_DCL_HS_FORK_PHASE_INSTANCE_COUNT] = "dcl_hs_fork_phase_instance_count",
    [VKD3DSIH_DCL_HS_JOIN_PHASE_INSTANCE_COUNT] = "dcl_hs_join_phase_instance_count",
    [VKD3DSIH_DCL_HS_MAX_TESSFACTOR           ] = "dcl_hs_max_tessfactor",
    [VKD3DSIH_DCL_IMMEDIATE_CONSTANT_BUFFER   ] = "dcl_immediateConstantBuffer",
    [VKD3DSIH_DCL_INDEX_RANGE                 ] = "dcl_index_range",
    [VKD3DSIH_DCL_INDEXABLE_TEMP              ] = "dcl_indexableTemp",
    [VKD3DSIH_DCL_INPUT                       ] = "dcl_input",
    [VKD3DSIH_DCL_INPUT_CONTROL_POINT_COUNT   ] = "dcl_input_control_point_count",
    [VKD3DSIH_DCL_INPUT_PRIMITIVE             ] = "dcl_inputPrimitive",
    [VKD3DSIH_DCL_INPUT_PS                    ] = "dcl_input_ps",
    [VKD3DSIH_DCL_INPUT_PS_SGV                ] = "dcl_input_ps_sgv",
    [VKD3DSIH_DCL_INPUT_PS_SIV                ] = "dcl_input_ps_siv",
    [VKD3DSIH_DCL_INPUT_SGV                   ] = "dcl_input_sgv",
    [VKD3DSIH_DCL_INPUT_SIV                   ] = "dcl_input_siv",
    [VKD3DSIH_DCL_INTERFACE                   ] = "dcl_interface",
    [VKD3DSIH_DCL_OUTPUT                      ] = "dcl_output",
    [VKD3DSIH_DCL_OUTPUT_CONTROL_POINT_COUNT  ] = "dcl_output_control_point_count",
    [VKD3DSIH_DCL_OUTPUT_SIV                  ] = "dcl_output_siv",
    [VKD3DSIH_DCL_OUTPUT_TOPOLOGY             ] = "dcl_outputTopology",
    [VKD3DSIH_DCL_RESOURCE_RAW                ] = "dcl_resource_raw",
    [VKD3DSIH_DCL_RESOURCE_STRUCTURED         ] = "dcl_resource_structured",
    [VKD3DSIH_DCL_SAMPLER                     ] = "dcl_sampler",
    [VKD3DSIH_DCL_STREAM                      ] = "dcl_stream",
    [VKD3DSIH_DCL_TEMPS                       ] = "dcl_temps",
    [VKD3DSIH_DCL_TESSELLATOR_DOMAIN          ] = "dcl_tessellator_domain",
    [VKD3DSIH_DCL_TESSELLATOR_OUTPUT_PRIMITIVE] = "dcl_tessellator_output_primitive",
    [VKD3DSIH_DCL_TESSELLATOR_PARTITIONING    ] = "dcl_tessellator_partitioning",
    [VKD3DSIH_DCL_TGSM_RAW                    ] = "dcl_tgsm_raw",
    [VKD3DSIH_DCL_TGSM_STRUCTURED             ] = "dcl_tgsm_structured",
    [VKD3DSIH_DCL_THREAD_GROUP                ] = "dcl_thread_group",
    [VKD3DSIH_DCL_UAV_RAW                     ] = "dcl_uav_raw",
    [VKD3DSIH_DCL_UAV_STRUCTURED              ] = "dcl_uav_structured",
    [VKD3DSIH_DCL_UAV_TYPED                   ] = "dcl_uav_typed",
    [VKD3DSIH_DCL_VERTICES_OUT                ] = "dcl_maxOutputVertexCount",
    [VKD3DSIH_DDIV                            ] = "ddiv",
    [VKD3DSIH_DEF                             ] = "def",
    [VKD3DSIH_DEFAULT                         ] = "default",
    [VKD3DSIH_DEFB                            ] = "defb",
    [VKD3DSIH_DEFI                            ] = "defi",
    [VKD3DSIH_DEQO                            ] = "deq",
    [VKD3DSIH_DFMA                            ] = "dfma",
    [VKD3DSIH_DGEO                            ] = "dge",
    [VKD3DSIH_DISCARD                         ] = "discard",
    [VKD3DSIH_DIV                             ] = "div",
    [VKD3DSIH_DLT                             ] = "dlt",
    [VKD3DSIH_DMAX                            ] = "dmax",
    [VKD3DSIH_DMIN                            ] = "dmin",
    [VKD3DSIH_DMOV                            ] = "dmov",
    [VKD3DSIH_DMOVC                           ] = "dmovc",
    [VKD3DSIH_DMUL                            ] = "dmul",
    [VKD3DSIH_DNE                             ] = "dne",
    [VKD3DSIH_DP2                             ] = "dp2",
    [VKD3DSIH_DP2ADD                          ] = "dp2add",
    [VKD3DSIH_DP3                             ] = "dp3",
    [VKD3DSIH_DP4                             ] = "dp4",
    [VKD3DSIH_DRCP                            ] = "drcp",
    [VKD3DSIH_DST                             ] = "dst",
    [VKD3DSIH_DSX                             ] = "dsx",
    [VKD3DSIH_DSX_COARSE                      ] = "deriv_rtx_coarse",
    [VKD3DSIH_DSX_FINE                        ] = "deriv_rtx_fine",
    [VKD3DSIH_DSY                             ] = "dsy",
    [VKD3DSIH_DSY_COARSE                      ] = "deriv_rty_coarse",
    [VKD3DSIH_DSY_FINE                        ] = "deriv_rty_fine",
    [VKD3DSIH_DTOF                            ] = "dtof",
    [VKD3DSIH_DTOI                            ] = "dtoi",
    [VKD3DSIH_DTOU                            ] = "dtou",
    [VKD3DSIH_ELSE                            ] = "else",
    [VKD3DSIH_EMIT                            ] = "emit",
    [VKD3DSIH_EMIT_STREAM                     ] = "emit_stream",
    [VKD3DSIH_ENDIF                           ] = "endif",
    [VKD3DSIH_ENDLOOP                         ] = "endloop",
    [VKD3DSIH_ENDREP                          ] = "endrep",
    [VKD3DSIH_ENDSWITCH                       ] = "endswitch",
    [VKD3DSIH_EQO                             ] = "eq",
    [VKD3DSIH_EQU                             ] = "eq_unord",
    [VKD3DSIH_EVAL_CENTROID                   ] = "eval_centroid",
    [VKD3DSIH_EVAL_SAMPLE_INDEX               ] = "eval_sample_index",
    [VKD3DSIH_EXP                             ] = "exp",
    [VKD3DSIH_EXPP                            ] = "expp",
    [VKD3DSIH_F16TOF32                        ] = "f16tof32",
    [VKD3DSIH_F32TOF16                        ] = "f32tof16",
    [VKD3DSIH_FCALL                           ] = "fcall",
    [VKD3DSIH_FIRSTBIT_HI                     ] = "firstbit_hi",
    [VKD3DSIH_FIRSTBIT_LO                     ] = "firstbit_lo",
    [VKD3DSIH_FIRSTBIT_SHI                    ] = "firstbit_shi",
    [VKD3DSIH_FRC                             ] = "frc",
    [VKD3DSIH_FREM                            ] = "frem",
    [VKD3DSIH_FTOD                            ] = "ftod",
    [VKD3DSIH_FTOI                            ] = "ftoi",
    [VKD3DSIH_FTOU                            ] = "ftou",
    [VKD3DSIH_GATHER4                         ] = "gather4",
    [VKD3DSIH_GATHER4_C                       ] = "gather4_c",
    [VKD3DSIH_GATHER4_C_S                     ] = "gather4_c_s",
    [VKD3DSIH_GATHER4_PO                      ] = "gather4_po",
    [VKD3DSIH_GATHER4_PO_C                    ] = "gather4_po_c",
    [VKD3DSIH_GATHER4_PO_C_S                  ] = "gather4_po_c_s",
    [VKD3DSIH_GATHER4_PO_S                    ] = "gather4_po_s",
    [VKD3DSIH_GATHER4_S                       ] = "gather4_s",
    [VKD3DSIH_GEO                             ] = "ge",
    [VKD3DSIH_GEU                             ] = "ge_unord",
    [VKD3DSIH_HCOS                            ] = "hcos",
    [VKD3DSIH_HS_CONTROL_POINT_PHASE          ] = "hs_control_point_phase",
    [VKD3DSIH_HS_DECLS                        ] = "hs_decls",
    [VKD3DSIH_HS_FORK_PHASE                   ] = "hs_fork_phase",
    [VKD3DSIH_HS_JOIN_PHASE                   ] = "hs_join_phase",
    [VKD3DSIH_HSIN                            ] = "hsin",
    [VKD3DSIH_HTAN                            ] = "htan",
    [VKD3DSIH_IADD                            ] = "iadd",
    [VKD3DSIH_IBFE                            ] = "ibfe",
    [VKD3DSIH_IDIV                            ] = "idiv",
    [VKD3DSIH_IEQ                             ] = "ieq",
    [VKD3DSIH_IF                              ] = "if",
    [VKD3DSIH_IFC                             ] = "ifc",
    [VKD3DSIH_IGE                             ] = "ige",
    [VKD3DSIH_ILT                             ] = "ilt",
    [VKD3DSIH_IMAD                            ] = "imad",
    [VKD3DSIH_IMAX                            ] = "imax",
    [VKD3DSIH_IMIN                            ] = "imin",
    [VKD3DSIH_IMM_ATOMIC_ALLOC                ] = "imm_atomic_alloc",
    [VKD3DSIH_IMM_ATOMIC_AND                  ] = "imm_atomic_and",
    [VKD3DSIH_IMM_ATOMIC_CMP_EXCH             ] = "imm_atomic_cmp_exch",
    [VKD3DSIH_IMM_ATOMIC_CONSUME              ] = "imm_atomic_consume",
    [VKD3DSIH_IMM_ATOMIC_EXCH                 ] = "imm_atomic_exch",
    [VKD3DSIH_IMM_ATOMIC_IADD                 ] = "imm_atomic_iadd",
    [VKD3DSIH_IMM_ATOMIC_IMAX                 ] = "imm_atomic_imax",
    [VKD3DSIH_IMM_ATOMIC_IMIN                 ] = "imm_atomic_imin",
    [VKD3DSIH_IMM_ATOMIC_OR                   ] = "imm_atomic_or",
    [VKD3DSIH_IMM_ATOMIC_UMAX                 ] = "imm_atomic_umax",
    [VKD3DSIH_IMM_ATOMIC_UMIN                 ] = "imm_atomic_umin",
    [VKD3DSIH_IMM_ATOMIC_XOR                  ] = "imm_atomic_xor",
    [VKD3DSIH_IMUL                            ] = "imul",
    [VKD3DSIH_INE                             ] = "ine",
    [VKD3DSIH_INEG                            ] = "ineg",
    [VKD3DSIH_ISFINITE                        ] = "isfinite",
    [VKD3DSIH_ISHL                            ] = "ishl",
    [VKD3DSIH_ISHR                            ] = "ishr",
    [VKD3DSIH_ISINF                           ] = "isinf",
    [VKD3DSIH_ISNAN                           ] = "isnan",
    [VKD3DSIH_ITOD                            ] = "itod",
    [VKD3DSIH_ITOF                            ] = "itof",
    [VKD3DSIH_ITOI                            ] = "itoi",
    [VKD3DSIH_LABEL                           ] = "label",
    [VKD3DSIH_LD                              ] = "ld",
    [VKD3DSIH_LD2DMS                          ] = "ld2dms",
    [VKD3DSIH_LD2DMS_S                        ] = "ld2dms_s",
    [VKD3DSIH_LD_RAW                          ] = "ld_raw",
    [VKD3DSIH_LD_RAW_S                        ] = "ld_raw_s",
    [VKD3DSIH_LD_S                            ] = "ld_s",
    [VKD3DSIH_LD_STRUCTURED                   ] = "ld_structured",
    [VKD3DSIH_LD_STRUCTURED_S                 ] = "ld_structured_s",
    [VKD3DSIH_LD_UAV_TYPED                    ] = "ld_uav_typed",
    [VKD3DSIH_LD_UAV_TYPED_S                  ] = "ld_uav_typed_s",
    [VKD3DSIH_LIT                             ] = "lit",
    [VKD3DSIH_LOD                             ] = "lod",
    [VKD3DSIH_LOG                             ] = "log",
    [VKD3DSIH_LOGP                            ] = "logp",
    [VKD3DSIH_LOOP                            ] = "loop",
    [VKD3DSIH_LRP                             ] = "lrp",
    [VKD3DSIH_LTO                             ] = "lt",
    [VKD3DSIH_LTU                             ] = "lt_unord",
    [VKD3DSIH_M3x2                            ] = "m3x2",
    [VKD3DSIH_M3x3                            ] = "m3x3",
    [VKD3DSIH_M3x4                            ] = "m3x4",
    [VKD3DSIH_M4x3                            ] = "m4x3",
    [VKD3DSIH_M4x4                            ] = "m4x4",
    [VKD3DSIH_MAD                             ] = "mad",
    [VKD3DSIH_MAX                             ] = "max",
    [VKD3DSIH_MIN                             ] = "min",
    [VKD3DSIH_MOV                             ] = "mov",
    [VKD3DSIH_MOVA                            ] = "mova",
    [VKD3DSIH_MOVC                            ] = "movc",
    [VKD3DSIH_MSAD                            ] = "msad",
    [VKD3DSIH_MUL                             ] = "mul",
    [VKD3DSIH_NEO                             ] = "ne_ord",
    [VKD3DSIH_NEU                             ] = "ne",
    [VKD3DSIH_NOP                             ] = "nop",
    [VKD3DSIH_NOT                             ] = "not",
    [VKD3DSIH_NRM                             ] = "nrm",
    [VKD3DSIH_OR                              ] = "or",
    [VKD3DSIH_ORD                             ] = "ord",
    [VKD3DSIH_PHASE                           ] = "phase",
    [VKD3DSIH_PHI                             ] = "phi",
    [VKD3DSIH_POW                             ] = "pow",
    [VKD3DSIH_QUAD_READ_ACROSS_D              ] = "quad_read_across_d",
    [VKD3DSIH_QUAD_READ_ACROSS_X              ] = "quad_read_across_x",
    [VKD3DSIH_QUAD_READ_ACROSS_Y              ] = "quad_read_across_y",
    [VKD3DSIH_QUAD_READ_LANE_AT               ] = "quad_read_lane_at",
    [VKD3DSIH_RCP                             ] = "rcp",
    [VKD3DSIH_REP                             ] = "rep",
    [VKD3DSIH_RESINFO                         ] = "resinfo",
    [VKD3DSIH_RET                             ] = "ret",
    [VKD3DSIH_RETP                            ] = "retp",
    [VKD3DSIH_ROUND_NE                        ] = "round_ne",
    [VKD3DSIH_ROUND_NI                        ] = "round_ni",
    [VKD3DSIH_ROUND_PI                        ] = "round_pi",
    [VKD3DSIH_ROUND_Z                         ] = "round_z",
    [VKD3DSIH_RSQ                             ] = "rsq",
    [VKD3DSIH_SAMPLE                          ] = "sample",
    [VKD3DSIH_SAMPLE_B                        ] = "sample_b",
    [VKD3DSIH_SAMPLE_B_CL_S                   ] = "sample_b_cl_s",
    [VKD3DSIH_SAMPLE_C                        ] = "sample_c",
    [VKD3DSIH_SAMPLE_C_CL_S                   ] = "sample_c_cl_s",
    [VKD3DSIH_SAMPLE_C_LZ                     ] = "sample_c_lz",
    [VKD3DSIH_SAMPLE_C_LZ_S                   ] = "sample_c_lz_s",
    [VKD3DSIH_SAMPLE_CL_S                     ] = "sample_cl_s",
    [VKD3DSIH_SAMPLE_GRAD                     ] = "sample_d",
    [VKD3DSIH_SAMPLE_GRAD_CL_S                ] = "sample_d_cl_s",
    [VKD3DSIH_SAMPLE_INFO                     ] = "sample_info",
    [VKD3DSIH_SAMPLE_LOD                      ] = "sample_l",
    [VKD3DSIH_SAMPLE_LOD_S                    ] = "sample_l_s",
    [VKD3DSIH_SAMPLE_POS                      ] = "sample_pos",
    [VKD3DSIH_SETP                            ] = "setp",
    [VKD3DSIH_SGE                             ] = "sge",
    [VKD3DSIH_SGN                             ] = "sgn",
    [VKD3DSIH_SINCOS                          ] = "sincos",
    [VKD3DSIH_SLT                             ] = "slt",
    [VKD3DSIH_SQRT                            ] = "sqrt",
    [VKD3DSIH_STORE_RAW                       ] = "store_raw",
    [VKD3DSIH_STORE_STRUCTURED                ] = "store_structured",
    [VKD3DSIH_STORE_UAV_TYPED                 ] = "store_uav_typed",
    [VKD3DSIH_SUB                             ] = "sub",
    [VKD3DSIH_SWAPC                           ] = "swapc",
    [VKD3DSIH_SWITCH                          ] = "switch",
    [VKD3DSIH_SWITCH_MONOLITHIC               ] = "switch",
    [VKD3DSIH_SYNC                            ] = "sync",
    [VKD3DSIH_TAN                             ] = "tan",
    [VKD3DSIH_TEX                             ] = "texld",
    [VKD3DSIH_TEXBEM                          ] = "texbem",
    [VKD3DSIH_TEXBEML                         ] = "texbeml",
    [VKD3DSIH_TEXCOORD                        ] = "texcrd",
    [VKD3DSIH_TEXDEPTH                        ] = "texdepth",
    [VKD3DSIH_TEXDP3                          ] = "texdp3",
    [VKD3DSIH_TEXDP3TEX                       ] = "texdp3tex",
    [VKD3DSIH_TEXKILL                         ] = "texkill",
    [VKD3DSIH_TEXLDD                          ] = "texldd",
    [VKD3DSIH_TEXLDL                          ] = "texldl",
    [VKD3DSIH_TEXM3x2DEPTH                    ] = "texm3x2depth",
    [VKD3DSIH_TEXM3x2PAD                      ] = "texm3x2pad",
    [VKD3DSIH_TEXM3x2TEX                      ] = "texm3x2tex",
    [VKD3DSIH_TEXM3x3                         ] = "texm3x3",
    [VKD3DSIH_TEXM3x3DIFF                     ] = "texm3x3diff",
    [VKD3DSIH_TEXM3x3PAD                      ] = "texm3x3pad",
    [VKD3DSIH_TEXM3x3SPEC                     ] = "texm3x3spec",
    [VKD3DSIH_TEXM3x3TEX                      ] = "texm3x3tex",
    [VKD3DSIH_TEXM3x3VSPEC                    ] = "texm3x3vspec",
    [VKD3DSIH_TEXREG2AR                       ] = "texreg2ar",
    [VKD3DSIH_TEXREG2GB                       ] = "texreg2gb",
    [VKD3DSIH_TEXREG2RGB                      ] = "texreg2rgb",
    [VKD3DSIH_UBFE                            ] = "ubfe",
    [VKD3DSIH_UDIV                            ] = "udiv",
    [VKD3DSIH_UGE                             ] = "uge",
    [VKD3DSIH_ULT                             ] = "ult",
    [VKD3DSIH_UMAX                            ] = "umax",
    [VKD3DSIH_UMIN                            ] = "umin",
    [VKD3DSIH_UMUL                            ] = "umul",
    [VKD3DSIH_UNO                             ] = "uno",
    [VKD3DSIH_USHR                            ] = "ushr",
    [VKD3DSIH_UTOD                            ] = "utod",
    [VKD3DSIH_UTOF                            ] = "utof",
    [VKD3DSIH_UTOU                            ] = "utou",
    [VKD3DSIH_WAVE_ACTIVE_ALL_EQUAL           ] = "wave_active_all_equal",
    [VKD3DSIH_WAVE_ACTIVE_BALLOT              ] = "wave_active_ballot",
    [VKD3DSIH_WAVE_ACTIVE_BIT_AND             ] = "wave_active_bit_and",
    [VKD3DSIH_WAVE_ACTIVE_BIT_OR              ] = "wave_active_bit_or",
    [VKD3DSIH_WAVE_ACTIVE_BIT_XOR             ] = "wave_active_bit_xor",
    [VKD3DSIH_WAVE_ALL_BIT_COUNT              ] = "wave_all_bit_count",
    [VKD3DSIH_WAVE_ALL_TRUE                   ] = "wave_all_true",
    [VKD3DSIH_WAVE_ANY_TRUE                   ] = "wave_any_true",
    [VKD3DSIH_WAVE_IS_FIRST_LANE              ] = "wave_is_first_lane",
    [VKD3DSIH_WAVE_OP_ADD                     ] = "wave_op_add",
    [VKD3DSIH_WAVE_OP_IMAX                    ] = "wave_op_imax",
    [VKD3DSIH_WAVE_OP_IMIN                    ] = "wave_op_imin",
    [VKD3DSIH_WAVE_OP_MAX                     ] = "wave_op_max",
    [VKD3DSIH_WAVE_OP_MIN                     ] = "wave_op_min",
    [VKD3DSIH_WAVE_OP_MUL                     ] = "wave_op_mul",
    [VKD3DSIH_WAVE_OP_UMAX                    ] = "wave_op_umax",
    [VKD3DSIH_WAVE_OP_UMIN                    ] = "wave_op_umin",
    [VKD3DSIH_WAVE_PREFIX_BIT_COUNT           ] = "wave_prefix_bit_count",
    [VKD3DSIH_WAVE_READ_LANE_AT               ] = "wave_read_lane_at",
    [VKD3DSIH_WAVE_READ_LANE_FIRST            ] = "wave_read_lane_first",
    [VKD3DSIH_XOR                             ] = "xor",
};

struct vkd3d_msl_generator
{
    struct vsir_program *program;
    const struct vkd3d_shader_scan_descriptor_info1 *scan_descriptor_info;
    const struct vkd3d_shader_compile_info *compile_info;
    struct vkd3d_string_buffer_cache string_buffers;
    struct vkd3d_string_buffer *buffer;
    struct vkd3d_string_buffer *temp_string_buffer;
    struct vkd3d_shader_message_context *message_context;
    int indent_level;
};

static const char* VKD3D_PRINTF_FUNC(2, 3) 
msl_print_temp_string(struct vkd3d_msl_generator *gen, const char *format, ...) {
    const char* src = &gen->temp_string_buffer->buffer[gen->temp_string_buffer->content_size];
    va_list args;
    int ret;

    va_start(args, format);
    ret = vkd3d_string_buffer_vprintf(gen->temp_string_buffer, format, args);
    va_end(args);
    // this is kinda dirty
    gen->temp_string_buffer->buffer[gen->temp_string_buffer->content_size] = '\0';
    gen->temp_string_buffer->content_size+=1;
    // FIXME: unsafe! realloc may move the underlying buffer!
    // and we are ignoring the vprintf return value
    return src;
}

static const char* msl_print_writemask(struct vkd3d_msl_generator * gen, uint32_t mask)
{
    switch(mask) {
        case 0b0001:
            return "x";
        case 0b0010:
            return "y";
        case 0b0100:
            return "z";
        case 0b1000:
            return "w";
        case 0b0011:
            return "xy";
        case 0b0110:
            return "yz";
        case 0b1100:
            return "zw";
        case 0b0101:
            return "xz";
        case 0b1010:
            return "yw";
        case 0b1001:
            return "xw";
        case 0b0111:
            return "xyz";
        case 0b1110:
            return "yzw";
        case 0b1101:
            return "xzw";
        case 0b1011:
            return "xyw";
        case 0b1111:
            return "xyzw";
        default: {
            VKD3D_UNREACHABLE;
        }
    }
}

static const char* components = "x\0y\0z\0w";

static const char* msl_print_swizzle(struct vkd3d_msl_generator *gen, uint32_t writemask, uint32_t swizzle)
{
    return msl_print_temp_string(gen, "%s%s%s%s", 
        (writemask & VKD3DSP_WRITEMASK_0) ? &components[vsir_swizzle_get_component(swizzle, 0) << 1] : "",
        (writemask & VKD3DSP_WRITEMASK_1) ? &components[vsir_swizzle_get_component(swizzle, 1) << 1] : "",
        (writemask & VKD3DSP_WRITEMASK_2) ? &components[vsir_swizzle_get_component(swizzle, 2) << 1] : "",
        (writemask & VKD3DSP_WRITEMASK_3) ? &components[vsir_swizzle_get_component(swizzle, 3) << 1] : ""
    );
};

static const char* msl_print_swizzle_suffix(struct vkd3d_msl_generator *gen, uint32_t writemask, uint32_t swizzle)
{
    if(swizzle == VKD3D_SHADER_SWIZZLE(X, Y, Z, W) && writemask == 0b1111)
    {
        return "";
    }
    return msl_print_temp_string(gen, ".%s", msl_print_swizzle(gen, writemask, swizzle));
};

static const char* msl_print_register(struct vkd3d_msl_generator *gen, const struct vkd3d_shader_register *reg, bool expect_float);

static const char* msl_print_index(struct vkd3d_msl_generator *gen, const struct vkd3d_shader_register_index *idx)
{
    if(idx->rel_addr) 
    {
        // VKD3D_ASSERT(idx->rel_addr->reg.dimension == VSIR_DIMENSION_SCALAR);
        return msl_print_temp_string(gen, "%s.%s + %u", 
            msl_print_register(gen, &idx->rel_addr->reg, false),
            &components[vsir_swizzle_get_component(idx->rel_addr->swizzle, 0) << 1],
            idx->offset);
    }
    return msl_print_temp_string(gen, "%u", idx->offset);
}

static const char* msl_print_register(struct vkd3d_msl_generator *gen, const struct vkd3d_shader_register *reg, bool expect_float)
{
    switch(reg->type) {
        case VKD3DSPR_TEMP:
        {
            VKD3D_ASSERT(reg->idx_count == 1);
            return msl_print_temp_string(gen, expect_float ? "r[%d]": "ri[%d]", reg->idx[0].offset);
        }
        case VKD3DSPR_INPUT:
        {
            return msl_print_temp_string(gen, expect_float ? "v[%s]" : "vi[%s]", msl_print_index(gen, &reg->idx[0]));
        }
        case VKD3DSPR_OUTPUT:
        {
            return msl_print_temp_string(gen, expect_float ? "o[%s]" : "oi[%s]", msl_print_index(gen, &reg->idx[0]));
        }
        case VKD3DSPR_IDXTEMP:
        {
            return msl_print_temp_string(gen, expect_float ? "x%d[%d]" : "x%di[%d]", reg->idx[0].offset, reg->idx[1].offset);
        }
        case VKD3DSPR_IMMCONST:
        {
            VKD3D_UNREACHABLE;
        }
        case VKD3DSPR_CONSTBUFFER:
        {
            // idx0: resource id
            // idx1: resource reg
            // idx2: offset
            VKD3D_ASSERT(reg->idx_count == 3);
            return msl_print_temp_string(gen, expect_float ? "cb%sf[%s]": "cb%s[%s]",
                msl_print_index(gen, &reg->idx[1]),
                msl_print_index(gen, &reg->idx[2]));
        }
        case VKD3DSPR_IMMCONSTBUFFER:
        {
            // idx0: resource id
            // idx1: resource reg
            // idx2: offset
            VKD3D_ASSERT(reg->idx_count == 1);
            return msl_print_temp_string(gen, expect_float ? "icbf[%s]": "icb[%s]",
                msl_print_index(gen, &reg->idx[0]));
        }
        default: break;
    }
    return "UNHANDLED_REG";
}

static const char* msl_print_const(struct vkd3d_msl_generator *gen, const struct vkd3d_shader_src_param *src, uint32_t mask, bool expect_float) {
    if(expect_float)
    {
        switch(mask) {
        case 0b0001:
            return msl_print_temp_string(gen, "%ff", src->reg.u.immconst_f32[0]);
        case 0b0010:
            return msl_print_temp_string(gen, "%ff", src->reg.u.immconst_f32[1]);
        case 0b0100:
            return msl_print_temp_string(gen, "%ff", src->reg.u.immconst_f32[2]);
        case 0b1000:
            return msl_print_temp_string(gen, "%ff", src->reg.u.immconst_f32[3]);
        case 0b0011:
            return msl_print_temp_string(gen, "float2(%ff, %ff)",
                src->reg.u.immconst_f32[0],
                src->reg.u.immconst_f32[1]);
        case 0b0110:
            return msl_print_temp_string(gen, "float2(%ff, %ff)",
                src->reg.u.immconst_f32[1],
                src->reg.u.immconst_f32[2]);
        case 0b1100:
            return msl_print_temp_string(gen, "float2(%ff, %ff)",
                src->reg.u.immconst_f32[2],
                src->reg.u.immconst_f32[3]);
        case 0b0101:
            return msl_print_temp_string(gen, "float2(%ff, %ff)",
                src->reg.u.immconst_f32[0],
                src->reg.u.immconst_f32[2]);
        case 0b1010:
            return msl_print_temp_string(gen, "float2(%ff, %ff)",
                src->reg.u.immconst_f32[1],
                src->reg.u.immconst_f32[3]);
        case 0b1001:
            return msl_print_temp_string(gen, "float2(%ff, %ff)",
                src->reg.u.immconst_f32[0],
                src->reg.u.immconst_f32[3]);
        case 0b0111:
            return msl_print_temp_string(gen, "float3(%ff, %ff, %ff)",
                src->reg.u.immconst_f32[0],
                src->reg.u.immconst_f32[1],
                src->reg.u.immconst_f32[2]);
        case 0b1110:
            return msl_print_temp_string(gen, "float3(%ff, %ff, %ff)",
                src->reg.u.immconst_f32[1],
                src->reg.u.immconst_f32[2],
                src->reg.u.immconst_f32[3]);
        case 0b1101:
            return msl_print_temp_string(gen, "float3(%ff, %ff, %ff)",
                src->reg.u.immconst_f32[0],
                src->reg.u.immconst_f32[2],
                src->reg.u.immconst_f32[3]);
        case 0b1011:
            return msl_print_temp_string(gen, "float3(%ff, %ff, %ff)",
                src->reg.u.immconst_f32[0],
                src->reg.u.immconst_f32[1],
                src->reg.u.immconst_f32[3]);
        case 0b1111:
            return msl_print_temp_string(gen, "float4(%ff, %ff, %ff, %ff)",
                src->reg.u.immconst_f32[0],
                src->reg.u.immconst_f32[1],
                src->reg.u.immconst_f32[2],
                src->reg.u.immconst_f32[3]);
        default: {
            VKD3D_UNREACHABLE;
        }
        }
    }
    else {
        switch(mask) {
        case 0b0001:
            return msl_print_temp_string(gen, "0x%u", src->reg.u.immconst_u32[0]);
        case 0b0010:
            return msl_print_temp_string(gen, "0x%u", src->reg.dimension == VSIR_DIMENSION_SCALAR ? src->reg.u.immconst_u32[0]: src->reg.u.immconst_u32[1]);
        case 0b0100:
            return msl_print_temp_string(gen, "0x%u", src->reg.dimension == VSIR_DIMENSION_SCALAR ? src->reg.u.immconst_u32[0]: src->reg.u.immconst_u32[2]);
        case 0b1000:
            return msl_print_temp_string(gen, "0x%u", src->reg.dimension == VSIR_DIMENSION_SCALAR ? src->reg.u.immconst_u32[0]: src->reg.u.immconst_u32[3]);
        case 0b0011:
            return msl_print_temp_string(gen, "int2(0x%X, 0x%X)",
                src->reg.u.immconst_u32[0],
                src->reg.u.immconst_u32[1]);
        case 0b0110:
            return msl_print_temp_string(gen, "int2(0x%X, 0x%X)",
                src->reg.u.immconst_u32[1],
                src->reg.u.immconst_u32[2]);
        case 0b1100:
            return msl_print_temp_string(gen, "int2(0x%X, 0x%X)",
                src->reg.u.immconst_u32[2],
                src->reg.u.immconst_u32[3]);
        case 0b0101:
            return msl_print_temp_string(gen, "int2(0x%X, 0x%X)",
                src->reg.u.immconst_u32[0],
                src->reg.u.immconst_u32[2]);
        case 0b1010:
            return msl_print_temp_string(gen, "int2(0x%X, 0x%X)",
                src->reg.u.immconst_u32[1],
                src->reg.u.immconst_u32[3]);
        case 0b1001:
            return msl_print_temp_string(gen, "int2(0x%X, 0x%X)",
                src->reg.u.immconst_u32[0],
                src->reg.u.immconst_u32[3]);
        case 0b0111:
            return msl_print_temp_string(gen, "int3(0x%X, 0x%X, 0x%X)",
                src->reg.u.immconst_u32[0],
                src->reg.u.immconst_u32[1],
                src->reg.u.immconst_u32[2]);
        case 0b1110:
            return msl_print_temp_string(gen, "int3(0x%X, 0x%X, 0x%X)",
                src->reg.u.immconst_u32[1],
                src->reg.u.immconst_u32[2],
                src->reg.u.immconst_u32[3]);
        case 0b1101:
            return msl_print_temp_string(gen, "int3(0x%X, 0x%X, 0x%X)",
                src->reg.u.immconst_u32[0],
                src->reg.u.immconst_u32[2],
                src->reg.u.immconst_u32[3]);
        case 0b1011:
            return msl_print_temp_string(gen, "int3(0x%X, 0x%X, 0x%X)",
                src->reg.u.immconst_u32[0],
                src->reg.u.immconst_u32[1],
                src->reg.u.immconst_u32[3]);
        case 0b1111:
            return msl_print_temp_string(gen, "int4(0x%X, 0x%X, 0x%X, 0x%X)",
                src->reg.u.immconst_u32[0],
                src->reg.u.immconst_u32[1],
                src->reg.u.immconst_u32[2],
                src->reg.u.immconst_u32[3]);
        default: {
            VKD3D_UNREACHABLE;
        }
        }
    }
}

static const char* msl_print_src_param(struct vkd3d_msl_generator *gen, const struct vkd3d_shader_src_param *src, const struct vkd3d_shader_dst_param* dst, bool expect_float) {
    const char* reg_str, *swizzle_suffix_str;
    if(src->reg.type == VKD3DSPR_IMMCONST) {
        return msl_print_const(gen, src, dst->write_mask, expect_float);
    }
    reg_str = msl_print_register(gen, &src->reg, expect_float); 
    swizzle_suffix_str = msl_print_swizzle_suffix(gen, dst->write_mask, src->swizzle);
    if(src->modifiers == VKD3DSPSM_ABS)
        return msl_print_temp_string(gen, "fabs(%s%s)", reg_str, swizzle_suffix_str);
    if(src->modifiers == VKD3DSPSM_NEG)
        return msl_print_temp_string(gen, "-%s%s", reg_str, swizzle_suffix_str);
    if(src->modifiers == VKD3DSPSM_ABSNEG)
        return msl_print_temp_string(gen, "-fabs(%s%s)", reg_str, swizzle_suffix_str);
    return msl_print_temp_string(gen, "%s%s", reg_str, swizzle_suffix_str);
};

static const char* msl_print_dst_param(struct vkd3d_msl_generator *gen, const struct vkd3d_shader_dst_param* dst, bool expect_float) {
    const char* reg_str = msl_print_register(gen, &dst->reg, expect_float); 
    const char* mask_str = msl_print_writemask(gen, dst->write_mask);
    return msl_print_temp_string(gen, "%s.%s", reg_str, mask_str);
};

// static const char* msl_print_modifier(const char* expr, struct )

static void msl_print_infix_float_binop(struct vkd3d_msl_generator *gen, const struct vkd3d_shader_instruction *instruction, const char* op) {
    vkd3d_string_buffer_printf(gen->buffer, "%s = %s %s %s;\n", 
        msl_print_dst_param(gen, &instruction->dst[0], true),
        msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], true),
        op,
        msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], true)
    );
}

static void msl_print_infix_integer_binop(struct vkd3d_msl_generator *gen, const struct vkd3d_shader_instruction *instruction, const char* op) {
    vkd3d_string_buffer_printf(gen->buffer, "%s = %s %s %s;\n", 
        msl_print_dst_param(gen, &instruction->dst[0], false),
        msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], false),
        op,
        msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], false)
    );
}

static void msl_print_func_float_binop(struct vkd3d_msl_generator *gen, const struct vkd3d_shader_instruction *instruction, const char* op) {
    vkd3d_string_buffer_printf(gen->buffer, "%s = %s(%s,  %s);\n", 
        msl_print_dst_param(gen, &instruction->dst[0], true),
        op,
        msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], true),
        msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], true)
    );
}

static void msl_print_func_integer_binop(struct vkd3d_msl_generator *gen, const struct vkd3d_shader_instruction *instruction, const char* op) {
    vkd3d_string_buffer_printf(gen->buffer, "%s = %s(%s,  %s);\n", 
        msl_print_dst_param(gen, &instruction->dst[0], false),
        op,
        msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], false),
        msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], false)
    );
}

static void msl_print_condition(struct vkd3d_msl_generator *gen, 
    const struct vkd3d_shader_src_param * value, 
    uint32_t condition_flag)
{
    struct vkd3d_shader_dst_param dummy_dst;
    dummy_dst.modifiers = 0;
    vsir_register_init(&dummy_dst.reg, VKD3DSPR_NULL, VKD3D_DATA_INT, 0);
    dummy_dst.shift = 0;
    dummy_dst.write_mask = 0b1;
    switch (condition_flag)
    {
        case VKD3D_SHADER_CONDITIONAL_OP_NZ: vkd3d_string_buffer_printf(gen->buffer, "%s != 0", msl_print_src_param(gen, value, &dummy_dst, false)); break;
        case VKD3D_SHADER_CONDITIONAL_OP_Z:  vkd3d_string_buffer_printf(gen->buffer, "%s == 0", msl_print_src_param(gen, value, &dummy_dst, false)); break;
    }
}

static void msl_print_sample(struct vkd3d_msl_generator *gen,
    const struct vkd3d_shader_instruction *instruction
    ) {
    const struct vkd3d_shader_src_param* coord = &instruction->src[0];
    const struct vkd3d_shader_src_param* resource = &instruction->src[1];
    const struct vkd3d_shader_src_param* sampler = &instruction->src[2];
    struct vkd3d_shader_dst_param dummy_dst;
    
    VKD3D_ASSERT(resource->reg.type == VKD3DSPR_RESOURCE);
    VKD3D_ASSERT(sampler->reg.type == VKD3DSPR_SAMPLER);
    dummy_dst.modifiers = 0;
    vsir_register_init(&dummy_dst.reg, VKD3DSPR_NULL, VKD3D_DATA_FLOAT, 0);
    dummy_dst.shift = 0;
    dummy_dst.write_mask = 0b111;
    // TODO
    vkd3d_string_buffer_printf(gen->buffer, "%s = tex.sample(s0, %s, int2(%d, %d))%s;\n",
        msl_print_dst_param(gen, &instruction->dst[0], true),
        msl_print_src_param(gen, coord, &dummy_dst, true),
        instruction->texel_offset.u,
        instruction->texel_offset.v,
        msl_print_swizzle_suffix(gen, instruction->dst[0].write_mask, resource->swizzle));
}

static int vkd3d_msl_handle_instruction(struct vkd3d_msl_generator *gen,
        const struct vkd3d_shader_instruction *instruction)
{
    int ret = VKD3D_OK;

    switch (instruction->opcode)
    {
        case VKD3DSIH_DCL:
    	case VKD3DSIH_DCL_INPUT_SGV:
    	case VKD3DSIH_DCL_OUTPUT_SIV:
        case VKD3DSIH_DCL_CONSTANT_BUFFER:
        case VKD3DSIH_DCL_SAMPLER:
        case VKD3DSIH_DCL_INPUT_PS:
        case VKD3DSIH_DCL_OUTPUT:
        case VKD3DSIH_DCL_TEMPS:
        case VKD3DSIH_DCL_INPUT:
        case VKD3DSIH_DCL_INPUT_PS_SGV:
            return VKD3D_OK;
        case VKD3DSIH_IFC:
        case VKD3DSIH_BREAKC:
            VKD3D_UNREACHABLE;
        default:
            break;
    }

    switch (instruction->opcode)
    {
        case VKD3DSIH_ELSE:
        case VKD3DSIH_ENDLOOP:
        case VKD3DSIH_ENDIF:
            gen->indent_level--;
        default:
            break;
    }
    
    if(instruction->opcode != VKD3DSIH_NOP) vkd3d_string_buffer_printf(gen->buffer, "%*s", 4 * gen->indent_level, "");

    switch (instruction->opcode)
    {
        case VKD3DSIH_NOP:
            break;
        case VKD3DSIH_DISCARD:
            vkd3d_string_buffer_printf(gen->buffer, "discard_fragment();\n");
            break;
        case VKD3DSIH_LOOP:
            vkd3d_string_buffer_printf(gen->buffer, "for (;;) {\n");
            gen->indent_level++;
            break;

        case VKD3DSIH_ENDLOOP:
            vkd3d_string_buffer_printf(gen->buffer, "}\n");
            break;
        
        case VKD3DSIH_IF: {
            vkd3d_string_buffer_printf(gen->buffer, "if (");
            msl_print_condition(gen, &instruction->src[0], instruction->flags);
            vkd3d_string_buffer_printf(gen->buffer, ") {\n");
            gen->indent_level++;
            break;
        }
        case VKD3DSIH_BREAKP: {
            vkd3d_string_buffer_printf(gen->buffer, "if (");
            msl_print_condition(gen, &instruction->src[0], instruction->flags);
            vkd3d_string_buffer_printf(gen->buffer, ") break; \n");
            break;
        }
        case VKD3DSIH_CONTINUEP: {
            vkd3d_string_buffer_printf(gen->buffer, "if (");
            msl_print_condition(gen, &instruction->src[0], instruction->flags);
            vkd3d_string_buffer_printf(gen->buffer, ") continue; \n");
            break;
        }

        case VKD3DSIH_ELSE:
            vkd3d_string_buffer_printf(gen->buffer, "} else {\n");
            gen->indent_level++;
            break;

        case VKD3DSIH_ENDIF:
            vkd3d_string_buffer_printf(gen->buffer, "}\n");
            break;

        case VKD3DSIH_RET:
            vkd3d_string_buffer_printf(gen->buffer, "return;\n");
            break;
        case VKD3DSIH_CONTINUE:
            vkd3d_string_buffer_printf(gen->buffer, "continue;\n");
            break;
        case VKD3DSIH_BREAK:
            vkd3d_string_buffer_printf(gen->buffer, "break;\n");
            break;

        case VKD3DSIH_MOV:
            vkd3d_string_buffer_printf(gen->buffer, "%s = %s;\n", 
                msl_print_dst_param(gen, &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], true)
            );
            break;
        case VKD3DSIH_MOVC:
            vkd3d_string_buffer_printf(gen->buffer, "%s = select(%s,%s,%s);\n", 
                msl_print_dst_param(gen, &instruction->dst[0], true),
                "UNHANDELD_CONDITION",
                msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[2], &instruction->dst[0], true)
            );
            break;

        case VKD3DSIH_SAMPLE:
            msl_print_sample(gen, instruction);
            break;
        case VKD3DSIH_MAD:
            vkd3d_string_buffer_printf(gen->buffer, "%s = fma(%s, %s, %s);\n", 
                msl_print_dst_param(gen, &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[2], &instruction->dst[0], true)
            );
            break;
        case VKD3DSIH_DP2:
        {
            struct vkd3d_shader_dst_param dummy_dst;
            dummy_dst.modifiers = 0;
            vsir_register_init(&dummy_dst.reg, VKD3DSPR_NULL, VKD3D_DATA_FLOAT, 0);
            dummy_dst.shift = 0;
            dummy_dst.write_mask = 0b11;
            vkd3d_string_buffer_printf(gen->buffer, "%s = dot(%s, %s);\n", 
                msl_print_dst_param(gen, &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[0], &dummy_dst, true),
                msl_print_src_param(gen, &instruction->src[1], &dummy_dst, true)
            );
            break;
        }
        case VKD3DSIH_DP3:
        {
            struct vkd3d_shader_dst_param dummy_dst;
            dummy_dst.modifiers = 0;
            vsir_register_init(&dummy_dst.reg, VKD3DSPR_NULL, VKD3D_DATA_FLOAT, 0);
            dummy_dst.shift = 0;
            dummy_dst.write_mask = 0b111;
            vkd3d_string_buffer_printf(gen->buffer, "%s = dot(%s, %s);\n", 
                msl_print_dst_param(gen, &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[0], &dummy_dst, true),
                msl_print_src_param(gen, &instruction->src[1], &dummy_dst, true)
            );
            break;
        }
        case VKD3DSIH_DP4:
        {
            struct vkd3d_shader_dst_param dummy_dst;
            dummy_dst.modifiers = 0;
            vsir_register_init(&dummy_dst.reg, VKD3DSPR_NULL, VKD3D_DATA_FLOAT, 0);
            dummy_dst.shift = 0;
            dummy_dst.write_mask = 0b1111;
            vkd3d_string_buffer_printf(gen->buffer, "%s = dot(%s, %s);\n", 
                msl_print_dst_param(gen, &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[0], &dummy_dst, true),
                msl_print_src_param(gen, &instruction->src[1], &dummy_dst, true)
            );
            break;
        }
        case VKD3DSIH_ADD:
            msl_print_infix_float_binop(gen, instruction, "+");
            break;
        case VKD3DSIH_IADD:
            msl_print_infix_integer_binop(gen, instruction, "+");
            break;
        case VKD3DSIH_SUB:
            msl_print_infix_float_binop(gen, instruction, "-");
            break;
        case VKD3DSIH_MUL:
            msl_print_infix_float_binop(gen, instruction, "*");
            break;
        case VKD3DSIH_DIV:
            msl_print_infix_float_binop(gen, instruction, "/");
            break;
        case VKD3DSIH_MAX:
            msl_print_func_float_binop(gen, instruction, "fmax");
            break;
        case VKD3DSIH_IMAX:
            msl_print_func_float_binop(gen, instruction, "max");
            break;
        case VKD3DSIH_MIN:
            msl_print_func_integer_binop(gen, instruction, "fmin");
            break;
        case VKD3DSIH_IMIN:
            msl_print_func_integer_binop(gen, instruction, "min");
            break;

        case VKD3DSIH_AND:
            msl_print_infix_integer_binop(gen, instruction, "&");
            break;
        case VKD3DSIH_OR:
            msl_print_infix_integer_binop(gen, instruction, "|");
            break;
        case VKD3DSIH_XOR:
            msl_print_infix_integer_binop(gen, instruction, "^");
            break;
        case VKD3DSIH_EQO:
            vkd3d_string_buffer_printf(gen->buffer, "%s = sext32(%s == %s);\n", 
                msl_print_dst_param(gen, &instruction->dst[0], false),
                msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], true)
            );
            break;
        case VKD3DSIH_NEO:
            vkd3d_string_buffer_printf(gen->buffer, "%s = sext32(%s != %s);\n", 
                msl_print_dst_param(gen, &instruction->dst[0], false),
                msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], true)
            );
            break;
        case VKD3DSIH_GEO:
            vkd3d_string_buffer_printf(gen->buffer, "%s = sext32(%s >= %s);\n", 
                msl_print_dst_param(gen, &instruction->dst[0], false),
                msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], true)
            );
            break;
        case VKD3DSIH_LTO:
            vkd3d_string_buffer_printf(gen->buffer, "%s = sext32(%s < %s);\n", 
                msl_print_dst_param(gen, &instruction->dst[0], false),
                msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], true),
                msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], true)
            );
            break;
        case VKD3DSIH_EQU:
            vkd3d_string_buffer_printf(gen->buffer, "%s = sext32(%s == %s);\n", 
                msl_print_dst_param(gen, &instruction->dst[0], false),
                msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], false),
                msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], false)
            );
            break;
        case VKD3DSIH_NEU:
            vkd3d_string_buffer_printf(gen->buffer, "%s = sext32(%s != %s);\n", 
                msl_print_dst_param(gen, &instruction->dst[0], false),
                msl_print_src_param(gen, &instruction->src[0], &instruction->dst[0], false),
                msl_print_src_param(gen, &instruction->src[1], &instruction->dst[0], false)
            );
            break;
        default:  {
            vkd3d_string_buffer_printf(gen->buffer, "// %s\n", shader_opcode_names[instruction->opcode]);
        }
    }

    return ret;
}

static int vkd3d_msl_generator_init(struct vkd3d_msl_generator* gen,
        struct vsir_program *program, 
        const struct vkd3d_shader_scan_descriptor_info1 *scan_descriptor_info,
        const struct vkd3d_shader_compile_info *compile_info, 
        struct vkd3d_shader_message_context *message_context)
{
    memset(gen, 0, sizeof(*gen));
    gen->program = program;
    vkd3d_string_buffer_cache_init(&gen->string_buffers);
    gen->buffer = vkd3d_string_buffer_get(&gen->string_buffers);
    gen->temp_string_buffer = vkd3d_string_buffer_get(&gen->string_buffers);
    /* FIXME: HACK BEGIN */
    vkd3d_free(gen->temp_string_buffer->buffer);
    gen->temp_string_buffer->buffer_size = 0x1000;
    gen->temp_string_buffer->buffer = vkd3d_malloc(gen->temp_string_buffer->buffer_size);
    /* HACK END */
    gen->message_context = message_context;
    gen->scan_descriptor_info = scan_descriptor_info;
    gen->compile_info = compile_info;
    gen->indent_level = 1;

    return VKD3D_OK;
}

static void vkd3d_msl_generator_cleanup(struct vkd3d_msl_generator* gen)
{
    vkd3d_string_buffer_release(&gen->string_buffers, gen->buffer);
    vkd3d_string_buffer_release(&gen->string_buffers, gen->temp_string_buffer);
    vkd3d_string_buffer_cache_cleanup(&gen->string_buffers);
}

static int vkd3d_glsl_generator_generate(struct vkd3d_msl_generator *gen, struct vkd3d_shader_code *out)
{
    struct vsir_program* program;
    struct vkd3d_shader_instruction_array instructions;
    void *code;
    int i;
    enum vkd3d_result result = VKD3D_OK;
    program = gen->program;
    instructions = program->instructions;

    for (i = 0; i < instructions.count && result >= 0; ++i)
    {
        struct vkd3d_shader_instruction *instruction = &instructions.elements[i];
        result = vkd3d_msl_handle_instruction(gen, instruction);
        if(result != VKD3D_OK) return result;
    }

    if (TRACE_ON())
        vkd3d_string_buffer_trace(gen->buffer);

    if ((code = vkd3d_malloc(gen->buffer->buffer_size)))
    {
        memcpy(code, gen->buffer->buffer, gen->buffer->content_size);
        out->size = gen->buffer->content_size;
        out->code = code;
    }
    else return VKD3D_ERROR_OUT_OF_MEMORY;

    return VKD3D_OK;
}

int msl_compile(struct vsir_program *program, uint64_t config_flags,
        const struct vkd3d_shader_scan_descriptor_info1 *scan_descriptor_info,
        const struct vkd3d_shader_compile_info *compile_info,
        struct vkd3d_shader_code *out, struct vkd3d_shader_message_context *message_context)
{
    struct vkd3d_msl_generator generator;
    int ret;

    if ((ret = vsir_program_normalise(program, config_flags, compile_info, message_context)) < 0)
        return ret;

    vkd3d_msl_generator_init(&generator, program, scan_descriptor_info, compile_info, message_context);
    ret = vkd3d_glsl_generator_generate(&generator, out);
    vkd3d_msl_generator_cleanup(&generator);

    return VKD3D_OK;
}